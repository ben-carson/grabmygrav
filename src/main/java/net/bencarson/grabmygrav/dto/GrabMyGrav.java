package net.bencarson.grabmygrav.dto;

import net.bencarson.grabmygrav.common.Constants;
import net.bencarson.grabmygrav.common.GravRating;
import net.bencarson.grabmygrav.common.GravType;
import net.bencarson.grabmygrav.engine.GrabMyGravEngine;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;

/*
 * Author: Ben Carson
 * Date: 11.28.2012
 */

public class GrabMyGrav {

	private String gravURL;
	private String email;
	private GravRating rating;
	private GravType defaultImg;
	private int imageSize;
	private boolean forceDefault;
	private static Logger _logger = Logger.getLogger(GrabMyGrav.class);
	
	public GrabMyGrav(String email) {
		StringBuilder gravURLBuilder = new StringBuilder();
		_logger.debug("Default constructor. Using the insecure URL.");
		gravURLBuilder.append(Constants.INSECURE_URL);
		gravURLBuilder.append(GrabMyGravEngine.hashEmailForGravatar(email));
		gravURL = gravURLBuilder.toString();
	}
	
	public GrabMyGrav(String email, boolean makeSecure) {
		StringBuilder gravURLBuilder = new StringBuilder();
		if(makeSecure) {
			_logger.debug("Using the secure URL.");
			gravURLBuilder.append(Constants.SECURE_URL);
		} else {
			_logger.debug("Using the insecure URL.");
			gravURLBuilder.append(Constants.INSECURE_URL);
		}
		gravURLBuilder.append(GrabMyGravEngine.hashEmailForGravatar(email));
		gravURL = gravURLBuilder.toString();
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * By default, only 'G' rated images are displayed unless you indicate that you would like to 
	 * see higher ratings. Using the r= or rating= parameters, you may specify one of the following 
	 * ratings to request images up to and including that rating:	
	 * g: suitable for display on all websites with any audience type.
	 * pg: may contain rude gestures, provocatively dressed individuals, the lesser swear words, or mild violence.
	 * r: may contain such things as harsh profanity, intense violence, nudity, or hard drug use.
	 * x: may contain hardcore sexual imagery or extremely disturbing violence. 
	 */
	public void setRating(GravRating rating) {
		Validate.notNull(rating);
		_logger.debug("Allow pictures with a rating of " + rating.getGravRating() + " and lower.");
		this.rating = rating;
	}
	public GravRating getRating() {
		return rating;
	}

	/*
	 * If you'd prefer to use your own default image (perhaps your logo, a funny face, whatever),
	 *  then you can easily do so by supplying the URL to an image in the d= or default= parameter. 
	 *  The URL should be URL-encoded to ensure that it carries across correctly, for example:
	 *  
	 * <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=http%3A%2F%2Fexample.com%2Fimages%2Favatar.jpg" />
	 *
	 * When you include a default image, Gravatar will automatically serve up that image if there 
	 * is no image associated with the requested email hash.
	 * 
	 * In addition to allowing you to use your own image, Gravatar has a number of built in options
	 *  which you can also use as defaults. Most of these work by taking the requested email hash and
	 * using it to generate a themed image that is unique to that email address. To use these options, 
	 * just pass one of the following keywords as the d= parameter to an image request:
	 * -404: do not load any image if none is associated with the email hash, instead return an HTTP 404 (File Not Found) response
	 * -mm: (mystery-man) a simple, cartoon-style silhouetted outline of a person (does not vary by email hash)
	 * -identicon: a geometric pattern based on an email hash
	 * -monsterid: a generated 'monster' with different colors, faces, etc
	 * -wavatar: generated faces with differing features and backgrounds
	 * -retro: awesome generated, 8-bit arcade-style pixelated faces
	 * -blank: transparent PNG image
	 */
	public void setDefaultImg(GravType defaultImg) {
		Validate.notNull(defaultImg);
		_logger.debug("Missing image will default to "+ defaultImg.getGravType() + ".");
		this.defaultImg = defaultImg;
	}
	public GravType getDefaultImg() {
		return defaultImg;
	}

	/*
	 * If for some reason you wanted to force the default image to always load,
	 *  you can do that by using the f= or forcedefault= parameter, 
	 *  and setting its value to y.
	 */
	public void setForceDefault(boolean forceDefault) {
		_logger.debug("Default will be forced, regardless of image availability.");
		this.forceDefault = forceDefault;
	}
	public boolean isForceDefault() {
		return forceDefault;
	}
	
	/*
	 * By default, images are presented at 80px by 80px if no size parameter is supplied. You may request
	 * a specific image size, which will be dynamically delivered from Gravatar by using the s= or size=
	 * parameter and passing a single pixel dimension (since the images are square)
	 */
	public void setImageSize(int imgSize) {
		_logger.debug("Setting size of Gravatar DTO to " + imgSize + " x " + imgSize + " pixels.");
		this.imageSize = imgSize;
	}
	public int getImageSize() {
		return imageSize;
	}

	public String getGravURL() {
		return gravURL;
	}
	public void setGravURL(String gravURL) {
		this.gravURL = gravURL;
	}
}
