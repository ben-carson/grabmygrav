package net.bencarson.grabmygrav.exceptions;

public class InvalidGravSizeException extends Exception {

	private static final long serialVersionUID = 4486429152303796690L;
	private String exceptionMessage;
	
	public InvalidGravSizeException(String errorMsg) {
		exceptionMessage = errorMsg;
	}
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	public void setExceptionMessage(String errMsg) {
		exceptionMessage = errMsg;
	}

}
