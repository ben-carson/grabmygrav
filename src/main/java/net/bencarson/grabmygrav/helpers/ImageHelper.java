package net.bencarson.grabmygrav.helpers;

import org.apache.log4j.Logger;

public class ImageHelper {

	private static Logger _log = Logger.getLogger(ImageHelper.class);
	
	//only allow images from 1px to 512px, even though Gravatar supports up to 2048px 
	public static boolean isValidSize(int sizeToValidate) {
		boolean isValidSize = true;
		if(sizeToValidate <= 0) {
			isValidSize = false;
			_log.warn(sizeToValidate + " is too small. Please provide a value between 1 and 512.");
		}
		if(sizeToValidate > 512) {
			isValidSize = false;
			_log.warn(sizeToValidate + " is too big. Please provide a value between 1 and 512.");
		}
		return isValidSize;
	}
	
}
