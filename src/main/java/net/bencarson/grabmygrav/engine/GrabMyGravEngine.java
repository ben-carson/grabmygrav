package net.bencarson.grabmygrav.engine;

import java.util.ArrayList;

import net.bencarson.grabmygrav.common.GravRating;
import net.bencarson.grabmygrav.common.GravType;
import net.bencarson.grabmygrav.dto.GrabMyGrav;
import net.bencarson.grabmygrav.helpers.ImageHelper;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;

public class GrabMyGravEngine {
	
	private static Logger _logger = Logger.getLogger(GrabMyGravEngine.class);

	public static String hashEmailForGravatar(String email) {
		StringBuilder hashedEmail = new StringBuilder();
		hashedEmail.append(DigestUtils.md5Hex(email.trim().toLowerCase()));
		_logger.debug("The hased email is " + hashedEmail);
		//hashedEmail.append(".jpg");
		return hashedEmail.toString();
	}
	
	/*
	 * By default, images are presented at 80px by 80px if no size parameter is supplied. 
	 * You may request a specific image size, which will be dynamically delivered from 
	 * Gravatar by using the s= or size= parameter and passing a single pixel dimension 
	 * (since the images are square)
	 * You may request images anywhere from 1px up to 2048px, however note that many users have lower resolution images, so requesting larger sizes may result in pixelation/low-quality images.
	 */
	public static String getSizeAsParam(int size) {
		String sizeStr = new String();
		if(ImageHelper.isValidSize(size)) {
			sizeStr = new String("s="+size);
		}
		return sizeStr;
	}
	
	public static String getDefaultAsParam(GravType defaultVal) {
		String controlDefault = new String("d="+defaultVal.getGravType());
		return controlDefault;
	}
	

	public static String forceDefault() {
		return "f=y";
	}
	
	/*
	 * Gravatar allows users to self-rate their images so that they can indicate
	 *  if an image is appropriate for a certain audience. By default, only 'G' 
	 *  rated images are displayed unless you indicate that you would like to see
	 *   higher ratings. Using the r= or rating= parameters, you may specify one 
	 *   of the following ratings to request images up to and including that rating:
	 *   
	 *   -g: suitable for display on all websites with any audience type.
	 *   -pg: may contain rude gestures, provocatively dressed individuals, the lesser swear words, or mild violence.
	 *   -r: may contain such things as harsh profanity, intense violence, nudity, or hard drug use.
	 *   -x: may contain hardcore sexual imagery or extremely disturbing violence.
	 *   
	 *   If the requested email hash does not have an image meeting the requested 
	 *   rating level, then the default image is returned
	 */
	public static String getRatingAsParam(GravRating rating) {
		String controlRating = new String("r="+rating.getGravRating());
		return controlRating;
	}
	
	public static String getFullGravURL(GrabMyGrav gravDto) {
		return gravDto.getGravURL() + GrabMyGravEngine.renderGravURLParams(gravDto);
	}
	
	/*
	 * Author: Ben Carson
	 * Date: 11.28.2012
	 * This method combines all available Gravatar parameters and outputs them as a single string(builder)
	 * This can then be appended to the appropriate Gravatar URL prefix
	 */
	public static String renderGravURLParams(GrabMyGrav gravDto) {
		ArrayList<String> paramsList = new ArrayList<String>();
		Validate.notNull(gravDto);
		
		if(gravDto.isForceDefault()) {
			paramsList.add(GrabMyGravEngine.forceDefault());
		}
		if(gravDto.getDefaultImg() != null) {
			paramsList.add(GrabMyGravEngine.getDefaultAsParam(gravDto.getDefaultImg()));
		}
		/*
		else {
			//force an avatar type so that the generic Gravatar 'G' icon
			paramsList.add(GrabMyGravEngine.setDefault(GravType.RETRO));
		}
		*/
		if(gravDto.getImageSize() > 0) {
			paramsList.add(GrabMyGravEngine.getSizeAsParam(gravDto.getImageSize()));
		}
		if(gravDto.getRating() != null) {
			paramsList.add(GrabMyGravEngine.getRatingAsParam(gravDto.getRating()));
		}

		return glueParamsTogether(paramsList);
	}
	
	private static String glueParamsTogether(ArrayList<String> gravParams) {
		String paramString = new String();
		if(ArrayUtils.isNotEmpty(gravParams.toArray())) {
			paramString = "?" + StringUtils.join(gravParams.iterator(), "&");
		}
		return paramString;
	}

	
}
