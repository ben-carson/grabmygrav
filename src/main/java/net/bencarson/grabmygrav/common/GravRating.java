package net.bencarson.grabmygrav.common;

public enum GravRating {

	GENERAL("g"),
	PARENTAL_GUIDANCE("pg"),
	RESTRICTED("r"),
	EXPLICIT("x");

	private String gravRating;
	
	public String getGravRating() {
		return gravRating;
	}
	
	GravRating(String ratingArg) {
		this.gravRating = ratingArg;
	}
	
}
