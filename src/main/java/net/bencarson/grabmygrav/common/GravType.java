package net.bencarson.grabmygrav.common;

public enum GravType {
	
	MONSTER("monsterid"),
	_404("404"),
	MYSTERYMAN("mm"),
	RETRO("retro"),
	IDENTICON("identicon"),
	WAVATAR("wavatar"),
	BLANK("blank");
	
	private String gravType;

	public String getGravType() {
		return gravType;
	}

	GravType(String gravType) {
		this.gravType = gravType;
	}
	
	@Override
	public String toString() {
		return getGravType();
	}

}
