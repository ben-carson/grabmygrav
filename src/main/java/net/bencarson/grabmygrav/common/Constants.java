package net.bencarson.grabmygrav.common;

public class Constants {
	
	public static final String INSECURE_URL = "http://s.gravatar.com/avatar/";
	public static final String SECURE_URL = "https://secure.gravatar.com/avatar/";
	
	public static final char DEFAULT_IMAGE_PARAM = 'd'; //also 'default'
	public static final char SIZE_PARAM = 's';			//also 'size'
	public static final char FORCE_DEFAULT_PARAM = 'f';	//also 'forcedefault'
	public static final char RATING_PARAM = 'r';		//also 'rating'
	
}
