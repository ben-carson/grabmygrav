package net.bencarson.test.grabmygrav.engine;

import net.bencarson.grabmygrav.common.GravRating;
import net.bencarson.grabmygrav.common.GravType;
import net.bencarson.grabmygrav.engine.GrabMyGravEngine;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class GrabMyGravEngineTest {

	String testEmail = "User@BenCarson.net";
	Logger _log = Logger.getLogger(GrabMyGravEngineTest.class);
	
	@Test
	public void testHashEmailForGravatar() {
		String actualEmail = GrabMyGravEngine.hashEmailForGravatar(testEmail);
		String expectedEmail = DigestUtils.md5Hex(testEmail.trim().toLowerCase());
		
		_log.debug("testHashEmail:");
		_log.debug("expected: " + expectedEmail);
		_log.debug("actual:   " + actualEmail);
		
		Assert.assertTrue(actualEmail + " vs. " + expectedEmail, actualEmail.equals(expectedEmail));
	}
	
	@Test
	public void testSecureHashEmailForGravatar() {
		String hashEmail = GrabMyGravEngine.hashEmailForGravatar(testEmail);
		String renderedEmail = DigestUtils.md5Hex(testEmail.trim().toLowerCase());
		Assert.assertTrue(hashEmail + " vs. " + renderedEmail, hashEmail.equals(renderedEmail));
	}
	
	@Test
	public void testSetSize() {
		String actualSize = GrabMyGravEngine.getSizeAsParam(100);
		String expectSize = new String("s=100");
		Assert.assertEquals(expectSize, actualSize);
	}
	
	@Test
	public void testSetDefault() {
		String actualDefault = GrabMyGravEngine.getDefaultAsParam(GravType.WAVATAR);
		String expectDefault = new String("d=wavatar");
		Assert.assertEquals(actualDefault, expectDefault);
	}
	
	@Test
	public void testForceDefault() {
		String actualForce = GrabMyGravEngine.forceDefault();
		String expectForce = new String("f=y");
		Assert.assertEquals(actualForce, expectForce);
	}
	
	@Test
	public void testSetRating() {
		String actualRating = GrabMyGravEngine.getRatingAsParam(GravRating.EXPLICIT);
		String expectRating = new String("r=x");
		Assert.assertEquals(actualRating, expectRating);
	}
	
}
