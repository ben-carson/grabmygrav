package net.bencarson.test.grabmygrav.dto;

import junit.framework.Assert;
import net.bencarson.grabmygrav.common.Constants;
import net.bencarson.grabmygrav.common.GravRating;
import net.bencarson.grabmygrav.common.GravType;
import net.bencarson.grabmygrav.dto.GrabMyGrav;
import net.bencarson.grabmygrav.engine.GrabMyGravEngine;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

public class GrabMyGravTest {

	Logger _log = Logger.getLogger(GrabMyGravTest.class);
	private static String testEmail = "ben@grncld.com";
	
	@Test
	public void testGrabMyTar() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		
		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		_log.debug(" VS. ");
		_log.debug(Constants.INSECURE_URL+DigestUtils.md5Hex(testEmail));
		Assert.assertEquals(GrabMyGravEngine.getFullGravURL(gravObj),Constants.INSECURE_URL+DigestUtils.md5Hex(testEmail));
		_log.debug("");
	}

	@Test
	public void testGrabMyTarSecure() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail,true);

		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		_log.debug(" VS. ");
		_log.debug( Constants.SECURE_URL+DigestUtils.md5Hex(testEmail));
		Assert.assertEquals(GrabMyGravEngine.getFullGravURL(gravObj), Constants.SECURE_URL+DigestUtils.md5Hex(testEmail));
		_log.debug("");
	}
	
	@Test
	public void testImageTooBig() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setImageSize(513);

		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("s=") <= 0 );
		_log.debug("");
	}
	
	@Test
	public void testImageTooSmall() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setImageSize(-3);

		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("s=") <= 0 );
		_log.debug("");
	}
	
	@Test
	public void testImageSize() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setImageSize(100);

		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("s=100") > 0 );
		_log.debug("");
	}

	@Test
	public void testForceDefault() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setForceDefault(true);
		
		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("f=y") > 0 );
		_log.debug("");
	}
	
	@Test
	public void testSpecifyDefault() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setDefaultImg(GravType.IDENTICON);
		
		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("d="+GravType.IDENTICON.getGravType()) > 0 );
		_log.debug("");
	}
	
	@Test
	public void testSpecifyRating() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail);
		gravObj.setRating(GravRating.PARENTAL_GUIDANCE);
		
		_log.debug(GrabMyGravEngine.getFullGravURL(gravObj));
		Assert.assertTrue( GrabMyGravEngine.getFullGravURL(gravObj).indexOf("r="+GravRating.PARENTAL_GUIDANCE.getGravRating()) > 0 );
		_log.debug("");
	}
	
	@Test
	public void testCompoundQuery() {
		GrabMyGrav gravObj = new GrabMyGrav(testEmail, true);
		gravObj.setRating(GravRating.PARENTAL_GUIDANCE);
		gravObj.setDefaultImg(GravType.RETRO);
		gravObj.setImageSize(100);
		StringBuilder expectedURL = new StringBuilder();
		expectedURL.append(gravObj.getGravURL());
		expectedURL.append(GrabMyGravEngine.renderGravURLParams(gravObj));
		_log.debug("Expected Compound URL: " + expectedURL.toString());
		String actualURL = GrabMyGravEngine.getFullGravURL(gravObj);
		_log.debug("Actual Compound URL:   " + actualURL);
		Assert.assertEquals(expectedURL.toString(), actualURL);
		
	}
	
}
